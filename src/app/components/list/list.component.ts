import { Component, OnInit } from '@angular/core';
import Business from '../../Business';
import { BusinessService } from '../../business.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  businesses: Business[];
  constructor(private bs: BusinessService) { }

  deleteBusiness(id) {
    if (confirm('Would you delete really?')) {
      this.bs.deleteBusiness(id).subscribe(res => {
        console.log('Deleted');
      })
    }
  }

  ngOnInit() {
    this.bs
      .getBusinesses()
      .subscribe((data: Business[]) => {
        this.businesses = data;
      });
  }

}
